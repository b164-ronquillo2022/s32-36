const jwt = require("jsonwebtoken");
const secret = "CrushAkoNgCrushKo";

//JSON Web Token or JWT is a way of securely passing information from the server to the front end or to other parts of server
//Information is kept secure through the use of secret code
//Only the system that knows the secret code that can decode the encrypted information


//Token Creation
/*
- Analogy
	Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	//When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};


	//Generate a JSON web token using the jwt's method (sign())

	return jwt.sign(data, secret, {})
}

//Token Verification

//analogy Receive the gift and open the lock to verify if the sender is legitimate


module.exports.verify = (req, res, next) => {
      // the token is retrieve from the request header 
     let token = req.headers.authorization;

     // token received and is not undefined
    if(typeof token !== "undefined"){

    	console.log(token);
       //Bearer 
       //the slice method takes only the token from the information sent via the request header
    	token = token.slice(7, token.length);

    	//validaton and decrypt

    	return jwt.verify(token,secret, (err, data) =>{
    		if(err) {
    			return res.send({ auth: "failed"})
    		}else {
    			next()
    		}
    	} )


    }else {
    	return res.send({ auth: "token undefined" })
    }
}

//Token decryption
//analogy- open gift get content
module.exports.decode = (token) => {
    //Token received and is not undefined
    if(typeof token !== "undefined") {
    	token = token.slice(7, token.length);


    	return jwt.verify(token, secret, (err, data) =>{
    		if (err) {
    			return null
    		} else {
    			return jwt.decode(token, { complete: true}).payload;
    		}
    	})
    } else {
    	//token does not exit
    	return null
    }
}
       





























       