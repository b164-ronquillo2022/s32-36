const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers")
const auth = require('../auth')

//Creating a course

router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})
// activity answer 

// router.post("/", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	isAdmin = userData.isAdmin;

// 	console.log(isAdmin);

// 	if (isAdmin) {

// 	courseController.addCourse(req.body).then(result => res.send(result));
// }	else{
	
// 	res.send(false);
// }


//});


//Retrieving ALL courses
router.get("/all", (req, res) =>{
	CourseController.getAllCourses().then(result => res.send(result));
});

//Retrieving all ACTIVE courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});

//Retrieving a SPECIFIC course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	CourseController.getCourse(req.params.courseId).then(result => res.send(result))
})

//Update a course

router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})



// Update isActive true to false
router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.archiveCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false);
	}
})





module.exports = router;